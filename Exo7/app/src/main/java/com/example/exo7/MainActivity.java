package com.example.exo7;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edt_saisie;
    private TextView txt_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt_view = findViewById(R.id.txt_view);
        edt_saisie = findViewById(R.id.edt_saisie);

        edt_saisie.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ChangementDeTexte();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void ChangementDeTexte()
    {
        String texte = edt_saisie.getText().toString();
        txt_view.setText(texte);
    }

    public void WriteTextItalic(View view) {
        txt_view.setTypeface(null,Typeface.NORMAL);
        ToItalic();
    }

    public void WriteTextBold(View view) {
        txt_view.setTypeface(null,Typeface.NORMAL);
        ToBold();
    }

    public void WriteTextUnderline(View view) {
        txt_view.setTypeface(null,Typeface.NORMAL);
        ToUnderline();
    }

    public void ToItalic()
    {
        txt_view.setPaintFlags(0);
        txt_view.setTypeface(null, Typeface.ITALIC);
    }

    public void ToUnderline()
    {
        txt_view.setPaintFlags(txt_view.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    public void ToBold()
    {
        txt_view.setPaintFlags(0);
        txt_view.setTypeface(null,Typeface.BOLD);
    }
}
